<h3>
	Insertar un producto
	-
	<small>
		<a href="index.php?p=productos.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if(isset($_POST['enviar'])){
	//Inserto el producto
	//Recojo los datos que quiero insertar
	$nombre=$_POST['nombre'];
	$descripcion=$_POST['descripcion'];	
	$precio=$_POST['precio'];

	//MUEVO la imagen a la carpeta
	//move_uploaded_file($_FILES['imagen']['tmp_name'], 'images/'.$nombreImagen);

	//Establezco consulta
	$sql="INSERT INTO productos(nombreProd, descripcionProd, precioProd)VALUES('$nombre', '$descripcion', '$precio')";

	//ejecuto la consulta
	if($consulta=$conexion->query($sql)){		
		header('Refresh: 2; url=index.php?p=productos.php');
		?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Realizado con exito
		</div>
		<?php
	}else{
		?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}

}else{
	//Muestro el formulario de insercion
?>

<form action="index.php?p=insertar.php" method="post">
	<div class="form-group">
		<label for="nombre">Nombre del producto:</label>
		<input type="text" class="form-control" name="nombre" id="nombre">
	</div>

	<div class="form-group">
		<label for="descripcion">Descripción del producto:</label>
		<textarea class="form-control" name="descripcion" id="descripcion"></textarea>
	</div>

	<div class="form-group">
		<label for="precio">Precio del producto:</label>
		<input type="number" min="0.00" step="0.01" name="precio" id="precio"/>		
	</div>

	<button type="submit" name="enviar" class="btn btn-default">
		Enviar
	</button>

</form> 

<?php } ?>