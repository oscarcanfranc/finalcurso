<h3>
	Modificar un producto
	-
	<small>
		<a href="index.php?p=productos.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if(isset($_POST['enviar'])){
	//modifico la noticia
	//Recojo los datos que quiero modificar
	$nombre=$_POST['nombre'];
	$descripcion=$_POST['descripcion'];	
	$precio=$_POST['precio'];
	$id=$_POST['idProducto'];

	//Establezco consulta
	$sql="UPDATE productos SET nombreProd='$nombre', descripcionProd='$descripcion' WHERE idProd=$id";

	//ejecuto la consulta
	if($consulta=$conexion->query($sql)){		
		header('Refresh: 2; url=index.php?p=productos.php');
?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Realizado con exito
		</div>
<?php } else { 	?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}

}else{
	//Muestro el formulario de modificacion
	//Necesito el id de la noticia que quiero modificar
	//para rellenar el formulario con los datos de dicha noticia
	$idProducto=$_GET['idProd'];
	$sql="SELECT * FROM productos WHERE idProd=$idProducto";
	$consulta=$conexion->query($sql);
	$registro=$consulta->fetch_array();
?>

<form action="index.php?p=modificar.php" method="post">
	<div class="form-group">
		<label for="nombre">Nombre del producto:</label>
		<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $registro['nombreProd']; ?>">
	</div>

	<div class="form-group">
		<label for="descripcion">Descripción del producto:</label>
		<textarea class="form-control" name="descripcion" id="descripcion"><?php echo $registro['descripcionProd']; ?></textarea>
	</div>

	<div class="form-group">
		<label for="precio">Precio del producto:</label>
		<input type="number" min="0.00" step="0.01" name="precio" id="precio" value="<?php echo $registro['precioProd']; ?>">	
	</div>

	<input type="hidden" name="idProducto" value="<?php echo $idProducto; ?>">

	<button type="submit" name="enviar" class="btn btn-default">
		Enviar
	</button>
</form> 

<?php } ?>