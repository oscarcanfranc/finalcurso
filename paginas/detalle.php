<h3>
	Detalle del producto - 
	<small>
		<a href="index.php?p=productos.php">Volver</a>
	</small>
</h3>

<?php  
//Recojo el id de Noticia que quiero mostrar
$idProd=$_GET['idProd'];

//Establezco una consulta segun ese id de Noticia
$sql="SELECT * FROM productos WHERE idProd=$idProd";

//Ejecuto la consulta
$consulta=$conexion->query($sql);

//Extraigo los datos de dicha consulta (Como solo tengo 1)
$registro=$consulta->fetch_array();
?>

<article>
	<header>
		<h4>
			<strong>
			<?php echo $registro['nombreProd']; ?>
			</strong>
		</h4>
	</header>

	<section>
		<?php echo $registro['descripcionProd']; ?>

	</section>
	<footer style="clear: both;">
		<?php echo $registro['precioProd']; ?> &euro;
	</footer>
</article>