<h2>
	Productos
</h2>
<hr>

<?php  
//Este archivo va a recibir una accion a realizar
//si no recibe una accion, por defecto quiero LISTAR elementos
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Pues dependiendo de $accion, la web hace una u otra cosa
switch($accion){
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'listado':
		if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=productos.php&accion=insertar">
				Insertar
			</a>
		</h4>
		<?php } ?>
		<hr>
		<table class="table table-striped table-hover">
		<tr>
			<th>Nombre del producto</th>
			<th>Acciones del producto</th>
		</tr>
		<?php 
		//Hacemos un listado con paginacion
		//Lo primero es saber el numero TOTAL		 
		$sql="SELECT * FROM productos";
		$consulta=$conexion->query($sql);
		$numeroTotalDeRegistros=$consulta->num_rows;

		//Establezco cuantos registros quiero por pagina
		$numeroDeRegistrosPorPagina=2; //a mano

		//Necesito RECOGER en que pagina estoy, para mostrar
		//unos resultados u otros
		if(isset($_GET['numeroDePagina'])){
			$numeroDePagina=$_GET['numeroDePagina'];
		}else{
			$numeroDePagina=0;
		}

		//Me las apa�o para hacer la consulta segun el 
		// $numeroDePagina que le paso
		$inicioLimite=$numeroDePagina*$numeroDeRegistrosPorPagina;

		$sql="SELECT * FROM productos LIMIT $inicioLimite,$numeroDeRegistrosPorPagina";

		//Ejecuto la consulta, esta vez con su LIMIT (paginado)
		$consulta=$conexion->query($sql);

		//Hacemos el bucle para recorrer los resultados
		while($registro=$consulta->fetch_array()){
			?>
			<tr>
				<td><?php echo $registro['nombreProd'] ?></td>
				<td>
					<a href="index.php?p=productos.php&accion=ver&id=<?php echo $registro['idProd']; ?>">Ver</a>
					<?php 
					if($_SESSION['conectado']){
					?>
					 - 
					<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $registro['idProd']; ?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>
					 - 
					<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $registro['idProd']; ?>">Modificar</a>
					<?php } ?>
				</td>
			</tr>
			<?php
		}
		?>
		</table>
		<hr>

		<div class="text-center">
		<ul class="pagination">
			
			<?php if($numeroDePagina==0){ ?>
				<li><a href="#">&laquo;</a></li>
			<?php }else{ ?>
				<li><a href=index.php?p=productos.php&accion=listado&numeroDePagina=<?php echo ($numeroDePagina-1); ?>>&laquo;</a></li>
			<?php } ?>
			
			<?php  
			//Calculo el numero total de paginas
			$numeroTotalDePaginas=ceil($numeroTotalDeRegistros/$numeroDeRegistrosPorPagina);
			
			//Uso un bucle para dibujar los numericos
			for($numPagina=0;$numPagina<$numeroTotalDePaginas;$numPagina++){

				if($numeroDePagina==$numPagina){
					$activa='active';
				}else{
					$activa='';
				}
			?>

			<li class="<?php echo $activa; ?>">
				<a href="index.php?p=productos.php&accion=listado&numeroDePagina=<?php echo $numPagina; ?>">
					<?php echo ($numPagina+1); ?>	
				</a>
			</li>
			
			<?php  
			} //Fin del bucle for
			?>

			<li><a href="#">&raquo;</a></li>
		</ul>
		</div>

		<?php

		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////   VER   //////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'ver':

		$id=$_GET['id'];
		$sql="SELECT * FROM productos WHERE idProd=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		?>
		<h4>
			<a href="index.php?p=productos.php&accion=listado">
				Volver
			</a>
		</h4>
		<hr>
		<article>
			
		<div class="jumbotron">
		<div class="container">
			<h1>
				<span class="glyphicon glyphicon-film"></span> 
				<?php echo $registro['nombreProd']; ?>	
			</h1>
			<p><?php echo $registro['descripcionProd']; ?></p>
			<hr>
			<p>
				<?php echo $registro['precioProd']; ?> Euros
				 - 
				<?php echo $registro['unidadesProd']; ?> unidades disponibles
			</p>
		</div>
		</div>

		</article>
		<?php

		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////  BORRAR  //////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'borrar':
		if($_SESSION['conectado']){
			$id=$_GET['id'];
			$sql="DELETE FROM productos WHERE idProd=$id";
			if($consulta=$conexion->query($sql)){
				//header('location:index.php?p=productos.php');
				header('Refresh: 2; url=index.php?p=productos.php');
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Realizado con exito
				</div>
				<?php
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	//////////////////  MODIFICAR  /////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'modificar':
		if($_SESSION['conectado']){
			if(isset($_POST['enviar'])){

				$nombreProd=$_POST['nombreProd'];
				$descripcionProd=$_POST['descripcionProd'];
				$precioProd=$_POST['precioProd'];
				$unidadesProd=$_POST['unidadesProd'];
				$idProd=$_POST['idProd'];

				$sql="UPDATE productos SET nombreProd='$nombreProd', descripcionProd='$descripcionProd', precioProd='$precioProd', unidadesProd='$unidadesProd' WHERE idProd='$idProd'";

				if($consulta=$conexion->query($sql)){
					//header('location:index.php?p=productos.php');
					header('Refresh: 2; url=index.php?p=productos.php');
					?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Realizado con exito
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
						<strong>ERROR!!</strong> 
						<?php echo $sql; ?>
					</div>
					<?php
				}

			}else{

				$id=$_GET['id'];
				$sql="SELECT * FROM productos WHERE idProd=$id";
				$consulta=$conexion->query($sql);
				$registro=$consulta->fetch_array();
			
				?>
				<form action="index.php?p=productos.php&accion=modificar" method="post">
					
					<div class="form-group">
						<label for="nombreProd">Nombre del producto:</label>
						<input type="text" class="form-control" name="nombreProd" id="nombreProd" value="<?php echo $registro['nombreProd']; ?>">
					</div>

					<div class="form-group">
						<label for="descripcionProd">Descripcion del producto:</label>
						<input type="text" class="form-control" name="descripcionProd" id="descripcionProd" value="<?php echo $registro['descripcionProd']; ?>">
					</div>

					<div class="form-group">
						<label for="precioProd">Precio del producto:</label>
						<input type="text" class="form-control" name="precioProd" id="precioProd" value="<?php echo $registro['precioProd']; ?>">
					</div>

					<div class="form-group">
						<label for="unidadesProd">Unidades del producto:</label>
						<input type="text" class="form-control" name="unidadesProd" id="unidadesProd" value="<?php echo $registro['unidadesProd']; ?>">
					</div>

					<input type="hidden" name="idProd" value="<?php echo $registro['idProd']; ?>">
					
					<input type="submit" class="form-control" name="enviar" value="enviar">

				</form>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;


	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	//////////////////  INSERTAR  /////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
			if(isset($_POST['enviar'])){

				$nombreProd=$_POST['nombreProd'];
				$descripcionProd=$_POST['descripcionProd'];
				$precioProd=$_POST['precioProd'];
				$unidadesProd=$_POST['unidadesProd'];

				$sql="INSERT INTO productos(nombreProd, descripcionProd, precioProd, unidadesProd)VALUES('$nombreProd', '$descripcionProd', '$precioProd', '$unidadesProd')";

				if($consulta=$conexion->query($sql)){
					//header('location:index.php?p=productos.php');
					header('Refresh: 2; url=index.php?p=productos.php');
					?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Realizado con exito
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
						<strong>ERROR!!</strong> 
						<?php echo $sql; ?>
					</div>
					<?php
				}

			}else{
			
				?>
				<form action="index.php?p=productos.php&accion=insertar" method="post">
					
					<div class="form-group">
						<label for="nombreProd">Nombre del producto:</label>
						<input type="text" class="form-control" name="nombreProd" id="nombreProd">
					</div>

					<div class="form-group">
						<label for="descripcionProd">Descripcion del producto:</label>
						<input type="text" class="form-control" name="descripcionProd" id="descripcionProd">
					</div>

					<div class="form-group">
						<label for="precioProd">Precio del producto:</label>
						<input type="text" class="form-control" name="precioProd" id="precioProd">
					</div>

					<div class="form-group">
						<label for="unidadesProd">Unidades del producto:</label>
						<input type="text" class="form-control" name="unidadesProd" id="unidadesProd">
					</div>
					
					<input type="submit" class="form-control" name="enviar" value="enviar">

				</form>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;

} //Fin del switch($accion)
?>